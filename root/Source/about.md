## about

By {AUTHOR}

posted on 04:57:33 AM 2018-06-18

Tags: Example, Documentation, Test

----------

mksite is a fast and compact static site generator. The issue is that it was too simplistic for my taste and lacked a few plug ins that I believe are essential, such as minifying html output.

Enter mksite v2.

Rather than simply relying on only bash and awk, I decided to use a few different tools to make it feel like a more complete static site generator. It has...

- a user defined text to html tool

	- by default cmark is used for really fast markdown to html conversion 

	- cmark can also avoid pharsing potentially dangerous HTML (this can potentially be nice for a static site generating service)

	- this can also be adapted to use pandoc or other tools to convert from markdown, restructured text, etc to html output

- built in webserver for local testing

- seperate templeting to easily read and create templates

	- header, menu, comments, footer 

	- the main content templeting can be placed in menu or comments file

- built in plug ins

	- plug ins use 'sed' command to find tag and replace them

	- already created plug ins include: 

		- {PAGE} (page name)

		- {DATE} (for today's date in UTC)

		- {GRAVATAR} (to give git user's gravatar url based on email)

		- {AUTHOR} (user name from git config)

		- {EMAIL} (user email from git config)

		- {GITREPO} (git url; variable defined)

		- {SITENAME} (name of site; variable defined)

		- {SITEURL} (URL of site; user defined)

		- {PGP} (PGP used from git config if any)

- attemps to organize file output from source from templates and other media assests

	- mksite by default outputs to the same directory (which can be a little messy)

	- 'Source' folder contains all assests and text files (css and images can be placed here)

	- 'Output' folder contains all generated and processed html (can publish it, rsync it, or however you wish)

	- 'Template' folder contains the 4 files needed to generate templates 

	- '$ bash generate.sh generate ; bash generate.sh http' will compile the site and start a testing webserver
