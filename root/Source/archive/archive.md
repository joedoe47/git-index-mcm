## archive

![avatar](https://www.gravatar.com/avatar/57d31237a16d306df30f99ee53220d2b?s=150)

Author: joedoe47 | Date: 10:12:34 PM | 2018-06-16

Tags: archive, example

----------

This folder is where older posts would go. Links from the index page won't be automatically updated if you move posts or pages here.

I plan to add a plug in that detects old archived files but for now just allowing users to pick and choose from a directory index from nginx or apache should work for now.

but I would like a nicer way to search through acrhived posts.
