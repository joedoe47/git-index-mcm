#we want to update any mirrors we have

test -d ${1} || { echo 'this is not a valid directory'; exit ; }

for X in ${1}/; do echo $X; cd $X; git remote update ; cd .. ; done

