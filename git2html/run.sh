#!/bin/bash 

#do any of these repositories have updates?
bash $(dirname $0)/update.sh /media/sdb1/gitolite/public/projects
bash $(dirname $0)/update.sh /media/sdb1/gitolite/public/academic
bash $(dirname $0)/update.sh /media/sdb1/gitolite/public/unlisted

#do we have any new repositories?
bash $(dirname $0)/mirror.sh /home/jms2/Documents/private/repolist/projects.txt
bash $(dirname $0)/mirror.sh /home/jms2/Documents/private/repolist/academic.txt
bash $(dirname $0)/mirror.sh /home/jms2/Documents/private/repolist/unlisted.txt

#we have the data. assemble the indexes
bash  $(dirname $0)/git.sh /media/sdb1/gitolite/public/projects /media/sdb1/www/git/projects projects ;  
bash  $(dirname $0)/git.sh /media/sdb1/gitolite/public/academic /media/sdb1/www/git/academic academic ; 
bash  $(dirname $0)/git.sh /media/sdb1/gitolite/public/unlisted /media/sdb1/www/git/unlisted unlisted ; 
