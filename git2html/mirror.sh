#We want to get a new mirror added to our list

test -f ${1} || { echo 'not a valid file!'; exit ; }

while read LINE ; do git clone --mirror git@gitlab.com:joedoe47/$LINE; done < ${1}
