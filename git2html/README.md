
# Static git log generator

I made this because:

- I wanted text files showing summaries and information about git projects

- Also wanted the ability to have a static site with said information for others to use and get information on projects 

- I wanted a static repository generator that I could make look much better than stagit or git-arr with only essential and or minimal javascript (also a static git repository browser that shows code signing status)


You want to use this because:

- Its fancier than stagit and git-arr to show case projects to managers and clients 

- You don't mind running this script via cron or git hook, rather than a dedicated service

- If gitlab, cgit, gitweb, etc are too slow to load under heavy traffic load

# Disclaimer

This is still a work in progress and I do plan to change how this operates. 

Please do not use in your production envioqrnment unless you have time and or are willing to modify the script to your needs.

There are certain things I am working on. 
1qq
- eg. changing how the projects are shown 

- eg. working on making the program keep track of previous snapshots and releases

- eg. making the work flow of the code as CPU efficent as possible (index generation needs touching up as well as just the code structure overall) 

However if you have a certain feature in mind, let me know via email, social media, and or github or gitlab.

# Requirements

Debian
````
~$ sudo apt install cmark sed bash cron
````

Fedora
````
~$ sudo dnf install cmark sed bash cron
````

# Useage

````
~$ bash git2md.sh /path/to/repositories /path/to/output 
````

and thats it. 

Just tell Apache or Nginx to serve "/path/to/output" and set a cron job or git hook to continually update git repos.

Eg. with nginx you can just use this simple oneliner directive

````
location ^~ / { alias /path/to/output; try_files $uri $uri/ =404; index index.html; autoindex on;}
````

This is awesome because with 1 line we can tell nginx to serve out "N" number of git projects. Also the reason for the autoindex parameter is to allow the client and or manager see the list of snapshots under "https://example.com/projectX/archive/". 
